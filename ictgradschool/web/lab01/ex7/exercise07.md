Herbivorous
* Cow
* Horse
* Sheep

Carnivorous
* Polar Bear
* Frog
* Fox
* Lion
* Rat
* Hawk

Omnivorous
* Cat
* Dog
* Chicken
* Fennec Fox
* Raccoon
* Capybaras